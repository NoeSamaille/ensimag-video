#include "synchro.h"
#include "ensitheora.h"

#include <semaphore.h>

bool fini;

void envoiTailleFenetre(th_ycbcr_buffer buffer)
{
    windowsx = buffer[0].width;
    windowsy = buffer[0].height;
    sem_post(&buffersize_mutex);
}

void attendreTailleFenetre()
{
    sem_wait(&buffersize_mutex);
}

void signalerFenetreEtTexturePrete()
{
    sem_post(&texture_window_mutex);
}

void attendreFenetreTexture()
{
    sem_wait(&texture_window_mutex);
}

void debutConsommerTexture()
{
    sem_wait(&read_texture);
    sem_wait(&texture_mutex);
}

void finConsommerTexture()
{
    sem_post(&texture_mutex);
    sem_post(&write_texture);
}

void debutDeposerTexture()
{
    sem_wait(&write_texture);
    sem_wait(&texture_mutex);
}

void finDeposerTexture()
{
    sem_post(&texture_mutex);
    sem_post(&read_texture);
}
