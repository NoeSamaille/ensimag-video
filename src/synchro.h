#ifndef SYNCHRO_H
#define SYNCHRO_H

#include <stdbool.h>
#include "ensitheora.h"
#include "ensivideo.h"

#include <semaphore.h>

extern bool fini;

// Semaphores
extern sem_t read_texture, write_texture;
extern sem_t buffersize_mutex, texture_window_mutex, texture_mutex;

void envoiTailleFenetre(th_ycbcr_buffer buffer);
void attendreTailleFenetre();

void attendreFenetreTexture();
void signalerFenetreEtTexturePrete();

void debutConsommerTexture();
void finConsommerTexture();

void debutDeposerTexture();
void finDeposerTexture();

#endif
