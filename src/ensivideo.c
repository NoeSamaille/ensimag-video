#include <stdio.h>
#include <unistd.h>
#include <assert.h>
#include <SDL2/SDL.h>

#include "stream_common.h"
#include "oggstream.h"
#include "ensitheora.h"

#include <pthread.h>
#include <semaphore.h>

int main(int argc, char *argv[])
{
    int res;

    if (argc != 2)
    {
        fprintf(stderr, "Usage: %s FILE", argv[0]);
        exit(EXIT_FAILURE);
    }
    assert(argc == 2);

    // Init exclusion mutuelle
    pthread_mutex_init(&theora_mutex, NULL);
    pthread_mutex_init(&vorbis_mutex, NULL);

    // Init synchro
    sem_init(&buffersize_mutex, 0, 0);
    sem_init(&texture_window_mutex, 0, 0);
    sem_init(&texture_mutex, 0, 1);
    sem_init(&read_texture, 0, 0);
    sem_init(&write_texture, 0, NBTEX);

    // Initialisation de la SDL
    res = SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_EVENTS);
    atexit(SDL_Quit);
    printf("err: %s", SDL_GetError());
    assert(res == 0);

    // start the two stream readers
    pthread_t theora_pid, vorbis_pid;

    pthread_create(&theora_pid, NULL, theoraStreamReader, argv[1]);
    pthread_create(&vorbis_pid, NULL, vorbisStreamReader, argv[1]);

    // wait audio thread
    pthread_join(vorbis_pid, NULL);

    // 1 seconde de garde pour le son,
    sleep(1);

    // tuer les deux threads videos si ils sont bloqués
    pthread_cancel(theora_pid);
    pthread_cancel(theora2sdlthread);

    // attendre les 2 threads videos
    pthread_join(theora_pid, NULL);
    pthread_join(theora2sdlthread, NULL);

    // Init exclusion mutuelle
    pthread_mutex_destroy(&theora_mutex);
    pthread_mutex_destroy(&vorbis_mutex);

    exit(EXIT_SUCCESS);
}
